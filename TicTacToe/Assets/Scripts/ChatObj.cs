﻿/**
 * Class: ChatObj
 * Purpose: Create an object that can be used for serializing and deserializing chat messages
 * Author: Caleb Moore
 * Date: 12/06/2018
 **/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChatObj{
    public string type;
    public string message;
    public string game_id;
    public string user_name;
    public string user_id;

    public ChatObj(string type, string message, string game_id, string user_name, string user_id)
    {
        this.type = type;
        this.message = message;
        this.game_id = game_id;
        this.user_name = user_name;
        this.user_id = user_id;
    }
}
