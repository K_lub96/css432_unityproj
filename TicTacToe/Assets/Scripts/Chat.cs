﻿/**
 * Class: Chat
 * Purpose: Handles lobby, and game chat
 * Author: Caleb Moore
 * Date: 12/06/2018
 **/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine.UI;
using Newtonsoft.Json;

public class Chat : MonoBehaviour {
    private Client client;
    public GameObject parentPanel;
    public InputField chatText;
    private string mode;

	private void Start()
	{
        client = GameObject.Find("ClientObj").GetComponent<Client>();
	}

    /**
    * Method: sendMessage
    * Purpose: Send a message to the server
    * Parameters: msg -> message to send to the server
    * Return Val: N/A
    * */
    public void sendMessage(Text msg){
        ChatObj newMsg = new ChatObj(mode, msg.text, client.getGameID(), client.getPlayerName(), client.getID());
        string msgToSend = JsonConvert.SerializeObject(newMsg);
        byte[] byteMsg = Encoding.ASCII.GetBytes(msgToSend);
        client.getSock().Send(byteMsg);
        chatText.text = "";
    }

    /**
    * Method: displayMessage
    * Purpose: Display message received from server
    * Parameters: newMsg -> Message received from server
    * Return Val: N/A
    * */
    public void displayMessage(ChatObj newMsg)
    {
        GameObject msgObj = (GameObject)Instantiate(Resources.Load("ChatMessage"));
        msgObj.transform.SetParent(parentPanel.transform, false);
        ChatMessage msg = msgObj.GetComponent<ChatMessage>();
        msg.setText(newMsg.user_name, newMsg.message);
    }

    /**
    * Method: setMode
    * Purpose: Set mode of chat
    * Parameters: mode -> either lobby chat or game chat
    * Return Val: N/A
    * */
    public void setMode(string mode){
        this.mode = mode;
    }

}
