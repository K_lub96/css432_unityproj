﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatMessage : MonoBehaviour {

    public Text playerName;
    public Text message;

    public void setText(string playerName, string message){
        this.playerName.text = playerName;
        this.message.text = message;
    }
	
}
